package com.training.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.github.javafaker.Faker;
import com.training.config.AppConfig;
import com.training.config.JPAConfig;
import com.training.model.Adresse;
import com.training.model.Employee;
import com.training.model.Notes;
import com.training.service.EmployeeService;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// @SuppressWarnings("resource")
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(AppConfig.class, JPAConfig.class);
		context.refresh();

		createUsers(context);
		EmployeeService empService = context.getBean(EmployeeService.class);

		List<Employee> empList = empService.findAll();

		Employee empLP = empService.findByLoginAndPassword("zubizarreta", "footluv");
		System.out.println("findByLoginAndPassword: " + empLP);

		/*
		 * @SuppressWarnings("deprecation") Page<Employee> empPage =
		 * empService.findAll(new PageRequest(0, 2));
		 */
		System.out.println("\n-------------- Liste de tous les employes --------------\n");
		for (Employee employee : empList) {
			System.out.println(employee);
			List<Adresse> getListAd = employee.getAdresse();
			for (Adresse adress : getListAd) {
				System.out.println(adress);
			}
		}

		System.out.println("\n-------------- Liste d'une page de 20 employes --------------\n");
		Page<Employee> empPageList = empService.findAll(PageRequest.of(0, 20));
		System.out.println(empPageList);
		for (Employee employe : empPageList) {
			System.out.println(employe);
		}

		// creates various emps, give emp id, notes, and context adds notes
		createSeverallNotes(context);

		System.out.println("\n-------------- Moyenne des notes d'un employe --------------\n");
		double moy = getMoyenneByIdAndAnnee(1, 2010, context);
		System.out.println("Moyenne of id=1 for 2010 " + moy);

		// I y a 5 employess avec des moyennes en 2010, la fonction doit en retourner 3
		System.out.println("\n-------------- Liste du top 3 employes par notes de l'annee 2010 --------------\n");
		int annee = 2010;
		List<Employee> emps = getThreeBestEmployees(annee, context);
		System.out.println("Le top 3 employe de l'annee 2010:");
		for (int i = 0; i < emps.size(); i++) {
			Employee curEmp = emps.get(i);
			System.out.println("le numero " + (i + 1) + " est " + curEmp.getPrenom() + " avec une moyenne de "
					+ curEmp.getMoyenneByAnnee(annee).getMoyenne());
		}

	}

	public static void createUsers(AnnotationConfigApplicationContext context) {
		// Creation employee
		createEmployee(0, context, "zubizarreta", "footluv");
		for (int i = 1; i < 100; i++) {
			createEmployee(i, context, "", "");

		}

	}

	public static void createEmployee(int i, AnnotationConfigApplicationContext context, String login, String pass) {
		Faker faker = new Faker();
		Employee emp = context.getBean(Employee.class);
		emp.setPrenom(faker.name().firstName());
		emp.setNom(faker.name().lastName());
		if (!login.isEmpty())
			emp.setLogin(login);
		else
			emp.setLogin(faker.name().username());
		if (!pass.isEmpty())
			emp.setPassword(pass);
		else
			emp.setPassword(faker.internet().password());
		// Ajout addresse

		emp.setEmail(faker.internet().emailAddress());
		emp.setRole("user");

		Adresse ad = context.getBean(Adresse.class);
		ad.setNumero_rue(Integer.parseInt(faker.address().streetAddressNumber()));
		ad.setRue(faker.address().streetName());
		ad.setCode_postale(faker.address().zipCode());
		ad.setVille(faker.address().city());
		ad.setPays(faker.address().country());

		ad.setEmployee(emp);

		Adresse ad2 = context.getBean(Adresse.class);
		ad2.setNumero_rue(Integer.parseInt(faker.address().streetAddressNumber()));
		ad2.setRue(faker.address().streetName());
		ad2.setCode_postale(faker.address().zipCode());
		ad2.setVille(faker.address().city());
		ad2.setPays(faker.address().country());

		ad2.setEmployee(emp);

		List<Adresse> adresseList = new ArrayList<Adresse>(Arrays.asList(ad, ad2));

		emp.setAdresse(adresseList);

		EmployeeService empService = context.getBean(EmployeeService.class);
		empService.createOrUpdateEmployee(emp);
	}

	public static void createSeverallNotes(AnnotationConfigApplicationContext context) {
		createNotes(1, new Notes(2009, 3, 5, 1, 4), context);
		createNotes(1, new Notes(2010, 3, 7, 8, 4), context);
		createNotes(1, new Notes(2012, 8, 5, 1, 0), context);

		createNotes(2, new Notes(2009, 6, 1, 1, 3), context);
		createNotes(34, new Notes(2010, 10, 10, 10, 4), context);

		createNotes(3, new Notes(2010, 0, 0, 0, 0), context);
		createNotes(4, new Notes(2010, 8, 4, 5, 4), context);
		createNotes(5, new Notes(2010, 1, 1, 1, 1), context);

	}

	public static void createNotes(int id, Notes note, AnnotationConfigApplicationContext context) {
		EmployeeService empService = context.getBean(EmployeeService.class);
		Employee emp = empService.findById(id);
		List<Notes> notes = new ArrayList<Notes>(Arrays.asList(note));
		note.setEmployee(emp);
		emp.setNotes(notes);

		empService.createOrUpdateEmployee(emp);
	}

	public static double getMoyenneByIdAndAnnee(int id, int annee, AnnotationConfigApplicationContext context) {
		EmployeeService empService = context.getBean(EmployeeService.class);

		Employee emp = empService.findByIdAndAnnee(id, annee);
		System.out.println("findbyIdAndNotesAnnee" + emp.getNotes());
		return emp.getMoyenneByAnnee(annee).getMoyenne();
	}

	public static List<Employee> getThreeBestEmployees(int annee, AnnotationConfigApplicationContext context) {
		EmployeeService empService = context.getBean(EmployeeService.class);
		Page<Employee> empsPage = empService.findAllByAnneeSortedByMoyenne(annee, 0, 3);
		List<Employee> emps = empsPage.getContent();

		return emps;
	}

}
