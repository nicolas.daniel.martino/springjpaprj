package com.training.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Component
@Scope("prototype")
@Table(name = "notes")
public class Notes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int annee;
	private int sport;
	private int social;
	private int performance;
	private int assiduite;
	private double moyenne;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;

	public Notes() {
		super();
	}

	public Notes(int annee, int sport, int social, int performance, int assiduite) {
		super();
		this.annee = annee;
		this.sport = sport;
		this.social = social;
		this.performance = performance;
		this.assiduite = assiduite;
		this.moyenne = this.calcMoyenne();
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public int getSport() {
		return sport;
	}

	public void setSport(int sport) {
		this.sport = sport;
	}

	public int getSocial() {
		return social;
	}

	public void setSocial(int social) {
		this.social = social;
	}

	public int getPerformance() {
		return performance;
	}

	public void setPerformance(int performance) {
		this.performance = performance;
	}

	public int getAssiduite() {
		return assiduite;
	}

	public void setAssiduite(int assiduite) {
		this.assiduite = assiduite;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public double calcMoyenne() {
		// TODO Auto-generated method stub
		return (new Double(sport + social + performance + assiduite)) / 5d;
	}

	public double getMoyenne() {
		return moyenne;
	}

	public void setMoyenne() {
		this.moyenne = calcMoyenne();
	}

	@Override
	public String toString() {
		return "Notes [id=" + id + ", annee=" + annee + ", sport=" + sport + ", social=" + social + ", performance="
				+ performance + ", assiduite=" + assiduite + ", moyenne=" + moyenne + "]";
	}
}
